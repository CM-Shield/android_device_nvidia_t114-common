#include <string.h>
#include <unistd.h>

typedef struct NvOsFileRec
{
  int fd;
  uint32_t type;
} NvOsFile;
typedef struct NvOsFileRec *NvOsFileHandle;

uint32_t NvOsIoctl(
  NvOsFileHandle hFile,
  uint32_t IoctlCode,
  void *pBuffer,
  uint32_t InBufferSize,
  uint32_t InOutBufferSize,
  uint32_t OutBufferSize )
{
  return 0x0003000f; //ioctl failed
}

int NvOsStrcmp(const char *s1, const char *s2)
{
  return strcmp(s1, s2);
}

int NvOsStrncmp(const char *s1, const char *s2, size_t size)
{
  return strncmp(s1, s2, size);
}

char *NvUStrstr(const char *s1, const char *s2)
{
  return strstr(s1, s2);
}

uint32_t NvULowestBitSet(uint32_t value, uint32_t size)
{
  uint32_t ret = 0;

  if (size > 16)
  {
    if(!(value & 0xffff))
    {
      ret += 16;
      value >>= 16;
    }
  }

  if (size > 8)
  {
    if (!(value & 0xff))
    {
      ret += 8;
      value >>= 8;
    }
  }

  if (!(value & 0xf))
  {
    ret += 4;
    value >>= 4;
  }

  if (!(value & 0x3))
  {
    ret += 2;
    value >>= 2;
  }

  return ret + ((value & 1) ? 0 : 1);
}

void *NvOsSetFileHooks(void *value)
{
  return NULL;
}
