LOCAL_PATH := $(call my-dir)
T124_SHIMS := ../../t124-common/shims

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(T124_SHIMS)/zygote_whitelist.cpp
LOCAL_C_INCLUDES := frameworks/base/core/jni \
                    system/core/base/include
ifneq ($(ZYGOTE_WHITELIST_PATH_EXTRA),)
    LOCAL_CFLAGS += -DPATH_WHITELIST_EXTRA=$(ZYGOTE_WHITELIST_PATH_EXTRA)
endif
LOCAL_SHARED_LIBRARIES := liblog
LOCAL_MODULE := libshim_zw
LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE           := libnvcpl
LOCAL_SRC_FILES        := dummy.cpp
LOCAL_SHARED_LIBRARIES := libnvcpl_vendor
LOCAL_VENDOR_MODULE    := true
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE           := libnvos_shim_t114
LOCAL_SRC_FILES        := nvos_shim.c
LOCAL_VENDOR_MODULE    := true
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE           := liby
LOCAL_SRC_FILES_32     := intrinsics_shim.s
LOCAL_SHARED_LIBRARIES := libm
LOCAL_LDFLAGS_arm      += -Wl,--version-script,$(LOCAL_PATH)/intrinsics_shim.arm.map
include $(BUILD_SHARED_LIBRARY)
