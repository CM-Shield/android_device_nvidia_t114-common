.extern __aeabi_idiv,__aeabi_idivmod,__aeabi_uidiv,__aeabi_uidivmod
.text
.thumb

.global s_aeabi_idiv
.type s_aeabi_idiv,%function
.thumb_func
s_aeabi_idiv:
    b __aeabi_idiv

.global s_aeabi_idivmod
.type s_aeabi_idivmod,%function
.thumb_func
s_aeabi_idivmod:
    b __aeabi_idivmod

.global s_aeabi_uidiv
.type s_aeabi_uidiv,%function
.thumb_func
s_aeabi_uidiv:
    b __aeabi_uidiv

.global s_aeabi_uidivmod
.type s_aeabi_uidivmod,%function
.thumb_func
s_aeabi_uidivmod:
    b __aeabi_uidivmod
