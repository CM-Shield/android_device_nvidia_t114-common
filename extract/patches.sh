# Copyright (C) 2021 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Raydiu uses an intrinsic that got moved around in Q, so it needs shimmed
function patch_raydium_t114() {
  sed -i 's/libm.so/liby.so/' ${LINEAGE_ROOT}/${OUTDIR}/t114/raydium/lib/librm31080.so

  sed -i 's/__aeabi_idiv/s_aeabi_idiv/'         ${LINEAGE_ROOT}/${OUTDIR}/t114/raydium/lib/librm31080.so
  sed -i 's/__aeabi_idivmod/s_aeabi_idivmod/'   ${LINEAGE_ROOT}/${OUTDIR}/t114/raydium/lib/librm31080.so
  sed -i 's/__aeabi_uidiv/s_aeabi_uidiv/'       ${LINEAGE_ROOT}/${OUTDIR}/t114/raydium/lib/librm31080.so
  sed -i 's/__aeabi_uidivmod/s_aeabi_uidivmod/' ${LINEAGE_ROOT}/${OUTDIR}/t114/raydium/lib/librm31080.so
}

patch_raydium_t114;
